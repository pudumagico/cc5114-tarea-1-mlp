import random
import time
import redNeural

with open("iris.data") as f:
    lines = f.readlines()
random.shuffle(lines)
with open("outfile.txt", "w+") as f:
    f.writelines(lines)

neuralNetwork = redNeural.redNeural(4,1)
neuralNetwork.addLayer(2)
neuralNetwork.addLayer(3)
neuralNetwork.addLayer(1)

epochs = 500
excersises = 50

neuralNetwork.initialize(epochs)

start = time.time()
for i in range(epochs):
    random.shuffle(lines)
    with open("outfile.txt", "w+") as f:
        f.writelines(lines)
    with open("outfile.txt") as f:
        for j in range(excersises):
            data = f.readline()
            splitdata = data.split(",")
            inputs = splitdata[0:4]
            realinputs = []
            for x in inputs:
                realinputs.append(float(x))
            targets = [int(splitdata[4])]
            neuralNetwork.train(1, realinputs, targets, i)

end = time.time()
print "Tiempo total de procesamiento"
print (end - start)
neuralNetwork.plotPerformance(epochs)
