Instrucciones de uso para Red Neural Multicapa:
1. Crear Red Neural especificando el numero de inputs y su tasa de aprendizaje.
2. Agregar tantas capas como estime conveniente especificando el numero de neuronas por cada capa.
3. Entrenar la red, usando una lista de inputs y otra lista de targets

Para mayor informacion referirse a los tests en la implementacion en redNeural.py
