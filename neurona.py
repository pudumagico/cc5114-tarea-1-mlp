import numpy as np
import matplotlib.pyplot as plt
import unittest

class neurona:
    def __init__(self, weights, bias, learnRate):
        self.weights = weights
        self.bias = bias
        self.learnRate = learnRate
        self.lastIntput = False
        self.lastOutput = False
        self.lastDelta = False

    def output(self, inputs):
        self.lastInput = inputs
        z = np.add(sum(np.multiply(self.weights, inputs)), self.bias)
        self.lastOutput = 1/(1+np.exp(-z))
        return 1/(1+np.exp(-z))

    def captureDelta(self,delta):
        self.lastDelta = delta

    def transferDerivative(self):
        return np.multiply(self.lastOutput, np.subtract(1.0, self.lastOutput))

    def weightXdelta(self, i):
        return np.multiply(self.lastDelta, self.weights[i])

    def learn(self):
        self.weights = np.add(self.weights, np.multiply(self.learnRate, np.multiply(self.lastDelta, self.lastInput)))
        self.bias = np.add(self.bias,np.multiply(self.learnRate,self.lastDelta))
