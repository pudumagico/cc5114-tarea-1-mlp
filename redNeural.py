import numpy as np
import matplotlib.pyplot as plt
import unittest
import capa
np.seterr(divide='ignore', invalid='ignore')


class redNeural:
    def __init__(self, numberOfInputs, learnRate):
        self.numberOfInputs = numberOfInputs
        self.learnRate = learnRate
        self.layers = []
        self.numberOfLayers = 0
        self.firstLayer = False
        self.lastLayer = False
        self.finalOutputs = []
        self.VPs = []
        self.VNs = []
        self.FPs = []
        self.FNs = []
        self.MAEs = []

    def addLayer(self, numberOfNeurons):
        if self.numberOfLayers == 0:
            layer = capa.capa(numberOfNeurons, self.numberOfInputs, self.learnRate)
            layer.isFirstLayer = True
            self.firstLayer = layer
            self.layers.append(layer)
            self.numberOfLayers += 1
        else:
            layer = capa.capa(numberOfNeurons, self.layers[-1].numberOfNeurons, self.learnRate)
            self.layers[-1].isLastLayer = False
            layer.isLastLayer = True
            layer.previousLayer = self.layers[-1]
            self.layers[-1].nextLayer = layer
            self.LastLayer = layer
            self.layers.append(layer)
            self.numberOfLayers += 1

    def train(self, trainexamples, input, target, epoch):
        i = 0

        threshold = 0.5
        output = self.layers[0].feed(input)

        if target[0] == 1:
            if output[i] >= threshold:
                self.VPs[epoch] += 1
            else:
                self.FNs[epoch] += 1
        else:
            if output[i] < threshold:
                self.VNs[epoch] += 1
            else:
                self.FPs[epoch] += 1

        # if output > 0.5 and target[i] == 1:
        #     self.VPs[epoch] += 1
        # elif output < 0.5 and target[i] == 1:
        #     self.FNs[epoch] += 1
        # elif output > 0.5 and target[i] == 0:
        #     self.FPs[epoch] += 1
        # else:
        #     self.VNs[epoch] += 1

        self.MAEs[epoch] += abs(np.subtract(output,target[i]))
        # if epoch+1%4==0:
        #     self.MAEs[epoch] = self.MAEs[epoch]/4
        i += 1
        if i == trainexamples:
            i = 0
        self.layers[-1].backPropagation(target)
        for layer in self.layers:
            layer.learn()
        return output

    def initialize(self, epochs):
        self.VPs = np.zeros(epochs)
        self.VNs = np.zeros(epochs)
        self.FPs = np.zeros(epochs)
        self.FNs = np.zeros(epochs)
        self.MAEs = np.zeros(epochs)

    def plotPerformance(self, N):
        x = np.arange(N)

        V = np.add(self.VPs, self.VNs)
        F = np.add(self.FPs, self.FNs)

        total = np.add(V, F)

        CC = np.divide(V,total)

        plt.plot(x, CC)
        plt.xlabel("epocas")
        plt.ylabel("% Correcto (Accuracy): VP + VN / Total")
        plt.show()

        plt.plot(x, self.MAEs)
        plt.xlabel("epocas")
        plt.ylabel("Error")
        plt.show()

        aux = np.add(self.VPs, self.FPs)
        P = np.divide(self.VPs, aux)

        plt.plot(x, P)
        plt.xlabel("epocas")
        plt.ylabel("Precision: VP / VP + FP")
        plt.show()

        aux = np.add(self.VPs, self.FNs)
        R = np.divide(self.VPs, aux)

        plt.plot(x, R)
        plt.xlabel("epocas")
        plt.ylabel("Recall: VP / VP + FN")
        plt.show()

        aux1 = np.divide(1,P)
        aux2 = np.divide(1,R)
        aux = np.add(aux1, aux2)
        F1 = np.divide(2,aux)

        plt.plot(x, F1)
        plt.xlabel("epocas")
        plt.ylabel("F1 SCORE")
        plt.show()

class PerceptronTestCase(unittest.TestCase):
    def runTest(self):

        N = 6000
        th = 0.5

        redsita = redNeural(2,1)
        redsita.addLayer(2)
        redsita.addLayer(3)
        redsita.addLayer(1)
        redsita.initialize(N)


        for i in range(N):
            if i == N-1:
                redsita.finalOutputs.append(redsita.train(1, [0,0], [0], i))
                redsita.finalOutputs.append(redsita.train(1, [0,1], [0], i))
                redsita.finalOutputs.append(redsita.train(1, [1,0], [0], i))
                redsita.finalOutputs.append(redsita.train(1, [1,1], [1], i))

                #Printeo del error
                redsita.plotPerformance(N)
                print "Outputs finales de la red"
                print ""
                print redsita.finalOutputs
            else:
                redsita.train(1, [0,0], [0], i)
                redsita.train(1, [0,1], [0], i)
                redsita.train(1, [1,0], [0], i)
                redsita.train(1, [1,1], [1], i)


        assert (redsita.finalOutputs[0][0]<th and redsita.finalOutputs[1][0]<th and redsita.finalOutputs[2][0]<th and redsita.finalOutputs[3][0]>th) == True, 'MLP AND fail'

        redsita = redNeural(2,1)
        redsita.addLayer(2)
        redsita.addLayer(3)
        redsita.addLayer(1)
        redsita.initialize(N)

        for i in range(N):
            if i == N-1:
                redsita.finalOutputs.append(redsita.train(1, [0,0], [0], i))
                redsita.finalOutputs.append(redsita.train(1, [0,1], [1], i))
                redsita.finalOutputs.append(redsita.train(1, [1,0], [1], i))
                redsita.finalOutputs.append(redsita.train(1, [1,1], [1], i))

                #Printeo del error
                redsita.plotPerformance(N)
                print "Outputs finales de la red"
                print ""
                print redsita.finalOutputs
            else:
                redsita.train(1, [0,0], [0], i)
                redsita.train(1, [0,1], [1], i)
                redsita.train(1, [1,0], [1], i)
                redsita.train(1, [1,1], [1], i)


        assert (redsita.finalOutputs[0][0]<th and redsita.finalOutputs[1][0]>th and redsita.finalOutputs[2][0]>th and redsita.finalOutputs[3][0]>th) == True, 'MLP OR fail'


        redsita = redNeural(2,1)
        redsita.addLayer(2)
        redsita.addLayer(3)
        redsita.addLayer(1)
        redsita.initialize(N)

        for i in range(N):
            if i == N-1:
                redsita.finalOutputs.append(redsita.train(1, [0,0], [0], i))
                redsita.finalOutputs.append(redsita.train(1, [0,1], [1], i))
                redsita.finalOutputs.append(redsita.train(1, [1,0], [1], i))
                redsita.finalOutputs.append(redsita.train(1, [1,1], [0], i))

                #Printeo del error
                redsita.plotPerformance(N)
                print "Outputs finales de la red"
                print ""
                print redsita.finalOutputs
            else:
                redsita.train(1, [0,0], [0], i)
                redsita.train(1, [0,1], [1], i)
                redsita.train(1, [1,0], [1], i)
                redsita.train(1, [1,1], [0], i)


        assert (redsita.finalOutputs[0][0]<th and redsita.finalOutputs[1][0]>th and redsita.finalOutputs[2][0]>th and redsita.finalOutputs[3][0]<th) == True, 'MLP XOR fail'


if __name__ == '__main__':
    unittest.main()



    # redsita2 = redNeural(8, 10)
    # redsita2.addLayer(2)
    # redsita2.addLayer(4)
    # for i in range(100):
    #     print i
    #     redsita2.train([0,0,0,1,1,0,1,1], [0,0,0,1])
